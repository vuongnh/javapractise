package Util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    private static final char[] HEX_ARRAY_LOWERCASE = {
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    private static final String FREE_MARKER_SYNTAX = "<(?=\\/?(#list|#if))";
    public static boolean isEmpty(String str) {
        return (str == null || str.trim().length() == 0);
    }


    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static String toString(Object obj) {
        if(obj == null)
            return "";
        return obj.toString().trim();
    }

    public static boolean isEmail(String email) {
        if (isEmpty(email))
            return false;
        final String EMAIL_PATTERN = "^[_a-zA-Z0-9-](\\.?([_a-zA-Z0-9-])+)*\\@[a-zA-Z0-9-](\\.?([a-zA-Z0-9-])+)*\\.([a-zA-Z0-9-])+$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public static String toLowerCase(String text) {
        try {
            return text.toLowerCase();
        } catch(Exception ex) { // check catch this Exception
            return "";
        }
    }
    public static String toUpperCase(String text) {
        if(text == null)
            return "";
        return text.toUpperCase();
    }

    public static String substring(String string, Integer maxLength) {
        if (string == null)
            return "";

        if (isEmpty(string) || string.length() <= maxLength)
            return string;

        return string.substring(0, maxLength);
    }


    public static boolean isIntegerNumber(String value){
        try{
            Integer.parseInt(value);
        }
        catch(NumberFormatException nfe){
            return false;
        }
        return true;
    }

    public static boolean isLatinh(String text) {
        if(vn.nextop.bo.util.common.StringUtil.isEmpty(text)){
            return false;
        }
        char[] cs = text.toCharArray();
        for (char c : cs) {
            if('\u0000' > c || c > '\u007F'){
                return false;
            }
        }
        return true;
    }

    public static boolean isMaxLength(String text, int max) {
        if (vn.nextop.bo.util.common.StringUtil.isEmpty(text)) {
            return false;
        }
        if (text.codePoints().count() <= max)
            return true;
        return false;
    }

    public static String printHex(byte[] bytes) {
        return printHex(bytes, HEX_ARRAY_LOWERCASE);
    }

    public static String printHex(byte[] bytes, char[] hexArray) {
        char[] hexChars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; ++i) {
            int v = bytes[i] & 0xFF;
            hexChars[i * 2] = hexArray[v >>> 4];
            hexChars[i * 2 + 1] = hexArray[v & 0x0F];
        }
        String answer = new String(hexChars);
        return answer;
    }

    public static String nullToEmpty(String s) {
        return s == null ? "" : s;
    }

    public static String rtrim(String s, int max) {
        if(s == null)
            return null;
        int le = s.length();
        if(le > max)
            s = s.substring(le - max, le);
        return s;
    }

    public static String rangeAppend(String first, String second, int maxRange) {
        StringBuilder builder = new StringBuilder();
        if (vn.nextop.bo.util.common.StringUtil.isEmpty(second)) {
            return first;
        }
        if (vn.nextop.bo.util.common.StringUtil.isEmpty(first)) {
            String r = vn.nextop.bo.util.common.StringUtil.rtrim(second, maxRange);
            return r;
        }
        builder.append(first);
        builder.append(", ");
        builder.append(second);
        String r = vn.nextop.bo.util.common.StringUtil.rtrim(builder.toString(), maxRange);
        return r;
    }

    public static boolean isSupportFreeMarker(String input){
        if(vn.nextop.bo.util.common.StringUtil.isEmpty(input)) return false;
        Pattern r = Pattern.compile(FREE_MARKER_SYNTAX);
        return r.matcher(input).find();
    }
}
