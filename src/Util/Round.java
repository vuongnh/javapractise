package Util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Round {

    public static void main(String[] args) {
        double a = round(100.36543541313154431565,3,RoundingMode.UP);
        System.out.println(new BigDecimal(a));

        BigDecimal b = roundBigDecimal(100.36543541313154431565,3,RoundingMode.UP);

        System.out.println(b);

    }
    public static double round(double value, int places, RoundingMode roundingMode) {
        if (places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, roundingMode);
        return bd.doubleValue();
    }
    public static BigDecimal roundBigDecimal(double value, int places, RoundingMode roundingMode) {
        if (places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, roundingMode);
        return bd;
    }
}
