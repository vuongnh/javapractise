package Util;

import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collection;

public final class Numbers {

    private Numbers() {}

    public static BigDecimal format(BigDecimal number, int scale, int round) {
        BigDecimal result = null;
        switch (round) {
            case 0:
                round = BigDecimal.ROUND_UP;
                break;
            case 1:
                round = BigDecimal.ROUND_DOWN;
                break;
            case 2:
                round = BigDecimal.ROUND_CEILING;
                break;
            case 3:
                round = BigDecimal.ROUND_FLOOR;
                break;
            case 4:
                round = BigDecimal.ROUND_HALF_UP;
                break;
            case 5:
                round = BigDecimal.ROUND_HALF_DOWN;
                break;
            case 6:
                round = BigDecimal.ROUND_HALF_EVEN;
                break;
            case 7:
                round = BigDecimal.ROUND_UNNECESSARY;
                break;
            default:
                round = BigDecimal.ROUND_UP;
                break;
        }
        if (number == null) {
            return result;
        }
        try {
            return number.setScale(scale, round);
        }
        catch (NumberFormatException e) {
        }
        return result;
    }


    public static String formatAmount(BigDecimal amount, int scale, int roundMode) {
        if (amount == null) return "";
        amount = format(amount, scale, roundMode);
        StringBuilder formatPattern = new StringBuilder("###,###");
        for (int i = 0; i < scale; i++) {
            if (i == 0) formatPattern.append(".");
            formatPattern.append("#");
        }
        return new DecimalFormat(formatPattern.toString()).format(amount);
    }

    public static String formatAmountPercent(BigDecimal number, int scale, RoundingMode mode, int minimumFractionDigits, int maximumFractionDigits) {
        if (number == null)
            return "";
        number = number.setScale(scale, mode);
        DecimalFormat df = new DecimalFormat("#,###.##%");
        df.setMaximumFractionDigits(maximumFractionDigits);
        df.setMinimumFractionDigits(minimumFractionDigits);

        String result = df.format(number);
        return result;
    }

    public static boolean isIntegerValue(BigDecimal bd) {
        return bd.stripTrailingZeros().scale() <= 0;
    }

    public static boolean isPositive(BigDecimal v) {
        if(v == null) return false;
        return v.compareTo(BigDecimal.ZERO) > 0;
    }

    public static BigDecimal add(BigDecimal v1, BigDecimal v2) {
        if(v1 == null && v2 == null) return null;
        if(v2 == null) return v1; return v1 == null ? v2 : v1.add(v2);
    }

    public static BigDecimal sum(final Collection<BigDecimal> values) {
        if(CollectionUtils.isEmpty(values)) return BigDecimal.ZERO;
        BigDecimal r = BigDecimal.ZERO; for(BigDecimal v : values) if(v != null) r = r.add(v);
        return r;
    }

}
