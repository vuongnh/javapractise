package Util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil1 {

    private boolean isSunday(String date) {
        Date currentDate = DateUtil1.toDate(date, DateUtil1.PATTERN_YYYYMMDD_BLANK);
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }
}
