package Util;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


public class DateUtil {

    public static final String PATTERN_YYMMDD = "yyyy/MM/dd";
    public static final String PATTERN_YYMMDD_HH_MM = "yyyy/MM/dd HH:mm";
    public static final String PATTERN_YYMMDD_BLANK = "yyyyMMdd";
    public static final String PATTERN_YYYYMMDD_BLANK = "yyyyMMdd";
    public static final String PATTERN_YYYYMd = "yyyy/M/d";
    public static final String PATTERN_YYYMMDD_HHMMSS_FULL_SLASH = "yyyy/MM/dd HH:mm:ss";
    public static final String PATTERN_SLASH_YYYYMMDD_HH_COLON_MM = "yyyy/MM/dd HH:mm";
    public static final String PATTERN_SLASH_YYYYMMDD_HH_COLON_MM_SS = "yyyy/MM/dd HH:mm:ss";
    public static final String PATTERN_FOR_GEN_ID = "MMddHHmm";
    public static final String PATTERN_YYYMMDD_HHMMSS = "yyyyMMdd HH:mm:ss";
    public static final String PATTERN_YYYMMDD_HHMMSS_FULL = "yyyy-MM-dd HH:mm:ss";
    public static final String PATTERN_YYMMDD_HHMMSS_BLANK = "yyyyMMddHHmmss";
    public static final String PATTERN_YYYYMM_BLANK = "yyyyMM";
    public static final String PATTERN_YYYYMM_DASH = "yyyy-MM";
    public static final String PATTERN_YYYYMM = "yyyy/MM";
    public static final String PATTERN_YYYY = "yyyy";
    public static final String PATTERN_HH_COLON_MM = "HH:mm";
    public static final String PATTERN_HHMMSS_BLANK = "HHmmss";
    public static final String PATTERN_YYMMDD_HHMMSSS="yyyy-MM-dd HH:mm:ss.S";
    public static final String PATTERN_YYYMMDD_HHMMSS_SEMI_SSS = "yyyyMMdd HH:mm:ss:SSS";
    public static final String PATTERN_YYYYMMDD_HHMMSS = "yyyyMMdd_HHmmss";
    public static final String PATTERN_YYYYMMDD_HH = "yyyyMMddHH";
    public static final String PATTERN_HHmmSSS = "HH:mm:ss.SSS";

    private static final ISOChronology CHRONOLOGY = ISOChronology.getInstance();

    protected final static java.time.format.DateTimeFormatter FORMATTER
            = java.time.format.DateTimeFormatter.ofPattern(DateUtil.PATTERN_HHmmSSS);

    public static int compare(String dateFrom, String dateTo, String pattern)
            throws ParseException {
        Date dFrom = toDate(dateFrom, pattern);
        Date dTo = toDate(dateTo, pattern);
        if (dFrom != null && dTo != null) {
            long t1 = dFrom.getTime();
            long t2 = dTo.getTime();

            if (t1 - t2 > 0) {
                return 1;
            } else if (t1 == t2) {
                return 0;
            } else
                return -1;
        } else {
            throw new ParseException(pattern, 0);
        }
    }

    public static String toString(long millis, String pattern) {
        return toString(new Date(millis), pattern);
    }

    public static String toString(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    public static String toString(Calendar date, String pattern) {
        if (date == null) {
            return "";
        }
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date.getTime());
    }

    public static Date toDate(String datetime, String pattern) {
        Date date = null;
        if (!StringUtil.isEmpty(datetime)) {
            try {
                DateFormat formatter = new SimpleDateFormat(pattern);
                formatter.setLenient(false);
                date = formatter.parse(datetime);
            } catch (ParseException e) {
                logger.error("Invalid format {}" , datetime);
            }
        }
        return date;
    }

    public static long convertUtc0AndLocal(long time, boolean toLocal) {
        // convert UTC 0 and time zone of server
        int offset = TimeZone.getDefault().getRawOffset();
        int h = offset / 3600000;
        int m = (offset / 60000) % 60;

        if (toLocal == false) { // local server time to UTC 0
            h = h * (-1);
            m = m * (-1);
        }
        time = DateUtil.addHours(time, h);
        time = DateUtil.addMinutes(time, m);
        return time;
    }

    public static long addDays(long millis, int delta) {
        return CHRONOLOGY.days().add(millis, delta);
    }

    public static long addHours(long millis, int delta) {
        return CHRONOLOGY.hours().add(millis, delta);
    }

    public static long addYears(long millis, int delta) {
        return CHRONOLOGY.years().add(millis, delta);
    }

    public static long addMonths(long millis, int delta) {
        return CHRONOLOGY.months().add(millis, delta);
    }

    public static long addMinutes(long millis, int delta) {
        return CHRONOLOGY.minutes().add(millis, delta);
    }

    public static long addSeconds(long millis, int delta) {
        return CHRONOLOGY.seconds().add(millis, delta);
    }

    public static Timestamp toTimestamp(String datetime, String pattern) {
        if (StringUtil.isEmpty(datetime))
            return null;
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat(pattern);
            formatter.setLenient(false);
            date = (Date) formatter.parse(datetime);
        } catch (ParseException e) {
            logger.warn("toTimestamp {}" , datetime);
        }

        if (date == null) {
            return null;
        }

        return new Timestamp(date.getTime());
    }

    public static Timestamp toEndOfTimestamp(String datetime, String pattern) {
        Timestamp timestamp = toTimestamp(datetime, pattern);
        if(timestamp == null) return null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return new Timestamp(cal.getTimeInMillis());
    }

    private static Calendar toCalendar(String datetime, String pattern) {
        if (StringUtil.isEmpty(datetime)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat(pattern);
            formatter.setLenient(false);
            date = (Date) formatter.parse(datetime);
        } catch (ParseException e) {
            logger.warn("parse date exception {}" , (datetime == null ? "null" : datetime));
        }

        if (date == null) {
            return calendar;
        }

        calendar.setTime(date);
        return calendar;
    }

    public static String convertBetweenDateFormat(String value, String sourceFormat, String destFormat) {
        if (!StringUtil.isEmpty(value)) {
            try {
                SimpleDateFormat sdfSource = new SimpleDateFormat(sourceFormat);
                Date date = sdfSource.parse(value);
                SimpleDateFormat sdfDestination = new SimpleDateFormat(destFormat);
                value = sdfDestination.format(date);
                return value;
            } catch (Exception e) { // check catch this Exception
                logger.warn("error: ", e);
            }
        }
        return null;
    }

    public static boolean isValidDate(String inDate, String pattern) {

        if (inDate == null)
            return false;

        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

        if (inDate.trim().length() != dateFormat.toPattern().length())
            return false;

        dateFormat.setLenient(false);

        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public static String getCurrentDateTime(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static String getPreviousDate(String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Calendar c1 = Calendar.getInstance();
        Date date = new Date();
        c1.setTime(date);
        c1.roll(Calendar.DAY_OF_YEAR, -1);
        return dateFormat.format(c1.getTime());
    }

    public static String getNextDate(String pattern, String dateParse) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Calendar c1 = Calendar.getInstance();
        DateFormat format = new SimpleDateFormat(PATTERN_YYMMDD_BLANK);
        Date date = format.parse(dateParse);
        c1.setTime(date);
        c1.roll(Calendar.DAY_OF_YEAR, 1);
        return dateFormat.format(c1.getTime());
    }

    public static String getOtherDateFromDate(String fromdate,int days, String patterm) {
        String targetDate = "";
        Calendar calendar = toCalendar(fromdate,patterm);
        if (calendar == null) {
            return targetDate;
        }
        calendar.add(Calendar.DAY_OF_MONTH, days);
        try {
            SimpleDateFormat formatter =  new SimpleDateFormat(patterm);
            targetDate = formatter.format(calendar.getTime());
        } catch (Exception e) {

        }
        return targetDate;
    }

    public static boolean checkValidDateTimeFormat(String dateTime, String pattern){
        Date date = toDate(dateTime, pattern);
        return date != null;
    }

    public static String toHourFormat(String input) {
        String[] array = input.split("(?<=\\G.{2})");
        return String.join(":", array);
    }

    public static String formatDate(String date) {
        StringBuilder builder = new StringBuilder();
        String year = date.substring(0, 4);
        builder.append(year);
        builder.append("/");
        String month = date.substring(4, 6);
        builder.append(month);
        builder.append("/");
        String days = date.substring(6, 8);
        builder.append(days);
        return builder.toString();
    }

    public static String roundMinute(String date) {
        DateTimeFormatter df = DateTimeFormat.forPattern(PATTERN_YYYMMDD_HHMMSS);
        return roundMinute(df.parseDateTime(date), df);
    }

    public static String roundMinute(long timeInMillis, String pattern) {
        DateTimeFormatter df = DateTimeFormat.forPattern(pattern);
        return roundMinute(new DateTime(timeInMillis), df);
    }

    public static String roundMinute(DateTime date, DateTimeFormatter df) {
        DateTime update = date.minus(date.getSecondOfMinute() * 1000 + date.getMillisOfSecond());
        String roundMin = df.print(update);
        return roundMin;
    }

    public static Date getStartDateOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getEndDateOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static String getTime(LocalTime time) {
        String timeStr = FORMATTER.format(time);
        return timeStr;
    }

    public static Timestamp convertToEndOfDay(String toDate){
        Calendar can = Calendar.getInstance();
        can.setTime(DateUtil.toDate(toDate, DateUtil.PATTERN_YYMMDD));
        can.add(Calendar.HOUR_OF_DAY, 23);
        can.add(Calendar.MINUTE,59);
        can.add(Calendar.MILLISECOND,59000);
        Timestamp toDateInput = new Timestamp(can.getTime().getTime());
        return toDateInput;
    }

    public static Date dateAdd(Date date, int field, int amount) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        c1.add(field, amount);
        return c1.getTime();
    }

    public static String getPreviousMonth() {
        StringBuilder previousMonth = new StringBuilder();

        LocalDate now = LocalDate.now();
        LocalDate earlier = now.minusMonths(1);

        int month = earlier.getMonth().getValue();
        StringBuilder monthValue = new StringBuilder();
        if (month < 10) {
            monthValue.append(0).append(month);
        } else {
            monthValue.append(month);
        }
        int year = earlier.getYear();
        previousMonth.append(year).append(monthValue);
        return previousMonth.toString();
    }

    public static boolean isFirstDayOfTheMonth() {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());

        return c.get(Calendar.DAY_OF_MONTH) == 1;
    }

    public static boolean checkFirstDayOfTheYear(String frontDate, String firstDateOfYear) {
        String firstDateYear = frontDate.substring(4, 8);
        return firstDateYear.equals(firstDateOfYear);
    }

    public static long getDiffTimeInSecond(DateTime date1, DateTime date2) {
        Seconds seconds = Seconds.secondsBetween(date1, date2);
        return seconds.getSeconds();
    }

    public static long getDiffTimeInSecond(DateTime date1, String date2String, String pattern) {
        DateTimeFormatter sdf = DateTimeFormat.forPattern(pattern);
        DateTime date = sdf.parseDateTime(date1.toString(pattern));
        DateTime date2 = sdf.parseDateTime(date2String);
        return getDiffTimeInSecond(date, date2);
    }

    public static long getDiffTimeInSecond(String date1String, DateTime date2, String pattern) {
        DateTimeFormatter sdf = DateTimeFormat.forPattern(pattern);
        DateTime date1 = sdf.parseDateTime(date1String);
        return getDiffTimeInSecond(date1, date2);
    }

    public static long getDiffTimeInSecond(String date1String, String date2String, String pattern) {
        DateTimeFormatter sdf = DateTimeFormat.forPattern(pattern);
        DateTime date1 = sdf.parseDateTime(date1String);
        DateTime date2 = sdf.parseDateTime(date2String);
        return getDiffTimeInSecond(date1, date2);
    }

    public static boolean validateBusinessDate(String date, String pattern, String fronDate) {
        try {
            if (DateUtil.compare(date, fronDate, DateUtil.PATTERN_YYMMDD_BLANK) < 0) {
                return false;
            }
            Calendar calendar = Calendar.getInstance();
            Date convertDate = DateUtil.toDate(date, pattern);
            calendar.setTime(convertDate);
            if (Calendar.SUNDAY == calendar.get(Calendar.DAY_OF_WEEK) || Calendar.SATURDAY == calendar.get(Calendar.DAY_OF_WEEK)) {
                return false;
            }
        } catch (ParseException e) {
            return false;
        }
        return true;

    }
}

