package Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public final class ListUtil {

    private ListUtil() {}

    public static  <I, O> List<O> newArrayList(Collection<I> coll, Function<I, O> mapper) {
        List<O> answer = new ArrayList<>();
        if (coll == null || coll.size() == 0)
            return answer;
        for(I item : coll)
            answer.add(mapper.apply(item));
        return answer;
    }

    public static <I,O> List<O> convertArrayToList(I[] array, Function<I,O> mapper) {
        List<O> list = new ArrayList<>();
        if (array == null || array.length == 0)
            return list;
        for(I input : array) {
            O output = mapper.apply(input);
            list.add(output);
        }
        return list;
    }

    public static <T> List<List<T>> separate(List<T> list, final int size) {
        List<List<T>> parts = new ArrayList<>();
        final int total = list.size();
        for (int i = 0; i < total; i += size) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(total, i + size)))
            );
        }
        return parts;
    }

    public static boolean isEmpty(List<?> list) {
        return list == null || list.isEmpty();
    }

    public static boolean isNotEmpty(List<?> list) {
        return !isEmpty(list);
    }

    public static <T> List<T> newArrayListFilter(List<T> list, Predicate<T> filter) {
        List<T> result = new ArrayList<>();
        if (isEmpty(list))
            return result;
        for (T t : list) {
            boolean valid = filter.test(t);
            if (valid)
                result.add(t);
        }
        return result;
    }

    public static <T> List<T> toList(T... values) {
        if(values == null) return new ArrayList<>(0);
        List<T> r = new ArrayList<T>(values.length);
        for (final T t : values) r.add(t); return r;
    }

    public static <T> List<T> toList(final T value) {
        List<T> r = new ArrayList<T>(1); add(r, value); return r;
    }

    public static <T> List<T> toList(Collection<? extends T> c) {
        return c == null ? new ArrayList<>(0) : new ArrayList<>(c);
    }

    public static <T> List<T> toList(final Iterator<T> iterator) {
        if(iterator == null) {
            return new ArrayList<>(0);
        } else {
            final List<T> r = new ArrayList<T>();
            while(iterator.hasNext()) r.add(iterator.next()); return r;
        }
    }

    public static <T> List<T> toList(Iterator<T> iterator, int limit) {
        if(iterator == null) {
            return new ArrayList<>(0);
        } else {
            final List<T> r = new ArrayList<T>(limit);
            while(iterator.hasNext()) r.add(iterator.next()); return r;
        }
    }

    public static <T> boolean add(final List<T> list, final T v) {
        if(list != null && v != null) return list.add(v); else return false;
    }
}
