package Util;

import org.apache.commons.lang3.StringUtils;
import vn.nextop.bo.util.log.nlogger.NLogger;
import vn.nextop.bo.util.log.nlogger.NLoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;


public class MathUtil {

	private static NLogger logger = NLoggerFactory.getLogger(vn.nextop.bo.util.common.MathUtil.class);

	public static Integer parseInteger(String str) {
		return parseInteger(str, null);
	}

	public static Integer parseInteger(String str, Integer defValue) {
		if (vn.nextop.bo.util.common.StringUtil.isEmpty(str))
			return defValue;
		try {
			return Integer.parseInt(str.trim());
		} catch (Exception e) { // check catch this Exception
			logger.warn("can't parse int from string: {}" , str);
			logger.warn("error: " , e);
			return defValue;
		}
	}

	public static Long parseLong(String str) {
		return parseLong(str, null);
	}

	public static Long parseLong(String str, Long defValue) {
		if (vn.nextop.bo.util.common.StringUtil.isEmpty(str))
			return defValue;
		try {
			return Long.parseLong(str.trim());
		} catch (Exception e) { // check catch this Exception
			logger.warn("can't parse long from string: {}" , str);
			logger.warn("error: " , e);
			return defValue;
		}
	}

	public static Double parseDouble(String str) {
		return parseDouble(str, null);
	}

	public static Double parseDouble(String str, Double defValue) {
		if (vn.nextop.bo.util.common.StringUtil.isEmpty(str))
			return defValue;
		StringBuffer bf = new StringBuffer(str);
		int index = bf.indexOf(",");
		while (index != -1) {
			bf.deleteCharAt(index);
			index = bf.indexOf(",");
		}
		try {
			return Double.parseDouble(bf.toString().trim());
		} catch (Exception e) { // check catch this Exception
			logger.warn("can't parse double from string: {}", str);
			logger.warn("error: ", e);
			return defValue;
		}
	}

	public static int parseInt(String str) {
		return parseInt(str, 0);
	}

	public static int parseInt(String str, int defValue) {
		if (vn.nextop.bo.util.common.StringUtil.isEmpty(str))
			return defValue;
		try {
			return Integer.parseInt(str.trim());
		} catch (Exception e) { // check catch this Exception
			logger.warn("can't parse int from string: {}" , str);
			logger.warn("error: " , e);
			return defValue;
		}
	}

	public static long parseLong(String str, long defValue) {
		if (vn.nextop.bo.util.common.StringUtil.isEmpty(str))
			return defValue;
		try {
			return Long.parseLong(str.trim());
		} catch (Exception e) { // check catch this Exception
			logger.warn("can't parse long from string: {}", str);
			logger.warn("error: ", e);
			return defValue;
		}
	}

	public static BigDecimal parseBigDecimal(Object obj) {
		if(obj == null)
			return new BigDecimal("0");
		String str = obj.toString().trim();
		if (vn.nextop.bo.util.common.StringUtil.isEmpty(str))
			return new BigDecimal("0");
		try {
			StringBuffer bf = new StringBuffer(str);
			int index = bf.indexOf(",");
			while (index != -1) {
				bf.deleteCharAt(index);
				index = bf.indexOf(",");
			}
			return new BigDecimal(bf.toString());
		} catch (Exception e) { // check catch this Exception
			logger.warn("can't parse BigDecimal from string: {}" , str);
			logger.warn("error: " , e);
			return new BigDecimal("0");
		}
	}

	public static String formatNumber(Number number,
                                      int decimalPlace,
                                      int roundingMode,
                                      boolean isFixedDecimalPlace, boolean isSeparateThousand) {
		//Start validate params
		if(number == null){
			number = new Double(0);
		}
		if(decimalPlace < 0){
			decimalPlace = 0;
		}
		if(roundingMode != BigDecimal.ROUND_CEILING || roundingMode != BigDecimal.ROUND_DOWN ||
			roundingMode != BigDecimal.ROUND_FLOOR || roundingMode != BigDecimal.ROUND_HALF_DOWN ||
			roundingMode != BigDecimal.ROUND_HALF_EVEN || roundingMode != BigDecimal.ROUND_HALF_UP ||
			roundingMode != BigDecimal.ROUND_UNNECESSARY || roundingMode != BigDecimal.ROUND_UP ){
			roundingMode = BigDecimal.ROUND_HALF_UP;
		}		
		//End validate params
		
		BigDecimal bd = new BigDecimal(number.toString());
		bd = bd.setScale(decimalPlace, roundingMode);
		String format = (isSeparateThousand ?  "#,": "") + "##0";
		String afterDecimalPlace = StringUtils.repeat(isFixedDecimalPlace ? '0':'#', decimalPlace);
		format = vn.nextop.bo.util.common.StringUtil.isEmpty(afterDecimalPlace) ? format : format + "." + afterDecimalPlace;
		DecimalFormat df = new DecimalFormat(format);
		return df.format(bd);
	}

	public static double doubleValue(BigDecimal c) {
		if (c == null)
			return 0;
		return c.doubleValue();
	}
}
