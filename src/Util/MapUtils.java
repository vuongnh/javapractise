package Util;

import org.apache.commons.lang.StringEscapeUtils;

import java.util.*;
import java.util.function.Function;

public final class MapUtils {

    public MapUtils() { }

    public static <K,V> Map<K,V> newHashMap(Map map, Function<V,K> keyGentor) {
        return newHashMap(map.values(), keyGentor);
    }

    public static <K,V> Map<K,V> newHashMap(Collection<V> coll, Function<V,K> keyGentor) {
        Map<K,V> map = new HashMap<>();
        for(V v : coll)
            map.put(keyGentor.apply(v), v);
        return map;
    }

    public static <V,K2,V2> Map<K2,V2> newHashMap(Collection<V> coll,
                                                  Function<V,K2> keyGentor,
                                                  Function<V,V2> valueGentor) {
        Map<K2,V2> map = new HashMap<>();
        for(V v : coll)
            map.put(keyGentor.apply(v), valueGentor.apply(v));
        return map;
    }

    public static <K,V> Map<K,V> removeAll(Map<K,V> map, Collection<K> keys) {
        Map<K, V> removed = new HashMap<>();
        for (K key : keys) {
            V remove = map.remove(key);
            if (remove != null)
                removed.put(key, remove);
        }
        return removed;
    }

    public static <K,V> Map<K, List<V>> newMultiMap(Collection<V> coll, Function<V,K> keyGentor) {
        Map<K, List<V>> mm = new HashMap<>();
        for(V value : coll) {
            List<V> list = mm.computeIfAbsent(keyGentor.apply(value), k -> new ArrayList<>());
            list.add(value);
        }
        return mm;
    }

    public static Map<Integer, String> escapeHtml(Map<Integer, String> input) {
        if (input == null) {
            return null;
        } else {
            Map<Integer, String> escapedMap = new HashMap();
            Iterator var2 = input.entrySet().iterator();

            while(var2.hasNext()) {
                Map.Entry<Integer, String> entry = (Map.Entry)var2.next();
                escapedMap.put(entry.getKey(), StringEscapeUtils.escapeHtml((String)entry.getValue()));
            }

            return escapedMap;
        }
    }

    public static boolean isEmpty(Map<?, ?> m) {
        return m == null || m.isEmpty();
    }
}
