package test;

import java.math.BigDecimal;

public class Test {
    private BigDecimal b1 = BigDecimal.ZERO;
    private BigDecimal b2 = BigDecimal.TEN;
    private BigDecimal b3 = BigDecimal.ONE;

    public BigDecimal getB1() {
        return b1;
    }

    public void setB1(BigDecimal b1) {
        this.b1 = b1;
    }

    public BigDecimal getB2() {
        return b2;
    }

    public void setB2(BigDecimal b2) {
        this.b2 = b2;
    }

    public BigDecimal getB3() {
        return b3;
    }

    public void setB3(BigDecimal b3) {
        this.b3 = b3;
    }
}
