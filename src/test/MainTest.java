package test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainTest {
    public static void main(String[] args) {
        Map<Integer,String> map1 = new HashMap<>();
        map1.put(1,"vuongnh");
        map1.put(2,"khanhnb");
        map1.put(3,"cuongln");
        map1.put(4,null);
        System.out.println("Map before change: " + map1);

        String t1 = map1.computeIfAbsent(1,k ->"tungtd");
        String t2 = map1.computeIfAbsent(4, k-> "tungtd");

        System.out.println("Map after change: " + map1);
        System.out.println(t1);
        System.out.println(t2);

        System.out.println("======================================== putIfAbsent");
        Map<Integer,String> map2 = new HashMap<>();
        map2.put(1,"vuongnh");
        map2.put(2,"khanhnb");
        map2.put(3,"cuongln");
        map2.put(4,null);
        System.out.println("Map before change: " + map2);

        String t3 = map2.putIfAbsent(1,"tungtd");
        String t4 = map2.putIfAbsent(4,"tungtd");

        System.out.println("Map after change: " + map2);
        System.out.println(t3);
        System.out.println(t4);



        System.out.println("=============================================test element in map and list is reference or value ???");
        Test test = new Test();

        System.out.println("b1 ===========" + test.getB1());
        System.out.println("b2 ===========" + test.getB2());
        System.out.println("b3 ===========" + test.getB3());

        // test break and return: break out from for loop and return is break out from method
        for (int i = 0; i <10 ; i++){
            if (i > 5){
                System.out.println(i);
                break;
            }
        }


        Map<String,Test> map = new HashMap<>();
        map.put("1",new Test());
        map.put("2",new Test());
        map.put("3",new Test());

        Test test1 = map.get("1");

        System.out.println("Map before change : "+ map.get("1").getB1());

        test1.setB1(BigDecimal.ONE);

        System.out.println("Map after change : "+ map.get("1").getB1());

        System.out.println("===========================> CONCLUSION: element in MAP is reference");

        List<Test> tests = new ArrayList<>();
        tests.add(new Test());
        tests.add(new Test());

        Test test2 = tests.get(0);
        System.out.println("List before change : "+ tests.get(0).getB1());

        test2.setB1(BigDecimal.ONE);
        System.out.println("List after change : "+ tests.get(0).getB1());

        System.out.println("===========================> CONCLUSION: element in LIST is reference");
    }
}
