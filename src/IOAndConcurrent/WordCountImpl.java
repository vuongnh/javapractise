package IOAndConcurrent;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordCountImpl implements Callable<Map<String,Integer>> {

    public static AtomicLong atomicLong = new AtomicLong(0);
    private String uri;

    public WordCountImpl(String uri) {
        this.uri = uri;
    }

    public WordCountImpl() {
    }
    //    public static void listFileFromDirectory(File files){
//        // read all file in folder before Java 8
//
////        for (File file : files.listFiles()){
////            if (file.isDirectory()){
////                listFileFromDirectory(file);
////            }else {
////                System.out.println(file.getName());
////            }
////        }
//
//        ///// Java 8
//        try(Stream<Path> file = Files.walk(files.toPath())){
//            List<String> filePaths = file.filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList());
//            for (String filePath : filePaths){
////                try(Stream<String> fileContent = Files.lines(Paths.get(filePath))){
////
////
////                }catch (Exception e){
////                    System.out.println(e);
////                }
//                System.out.println(filePath);
//            }
//        }catch (Exception e){
//            System.out.println(e);
//        }
//    }

    @Override
    public Map<String, Integer> call() {
        Map<String,Integer> map = new HashMap<>();
        try(Stream<String> fileContent = Files.lines(Paths.get(uri), StandardCharsets.ISO_8859_1)){

            fileContent.forEach( lines -> {
                String[] data = lines.split(" ");

//                for (int i = 0; i< data.length; i++){
//                    atomicLong.getAndIncrement();
//                    if (map.containsKey(data[i].trim())){
//                        map.put(data[i].trim(),map.get(data[i].trim()) + 1);
//                    }else{
//                        map.put(data[i].trim(),1);
//                    }
//                }
                Arrays.stream(data).forEach(word -> {
                    String formatWord = word.trim();

                    atomicLong.getAndIncrement();

//                    if (map.containsKey(formatWord)) {
//                        map.put(formatWord, map.get(word) + 1);
//                    } else {
//                        map.put(formatWord, 1);
//                    }
                    map.put(word,map.containsKey(word) ? map.get(word) + 1 : 1);
                });
            });

        }catch (Exception e){
            System.out.println(e.getCause());
        }
        return map;
    }

    //        List<String> words = new ArrayList<>();
//        words.add("Hi");
//        words.add("Hi");
//        words.add("Hi");
//        words.add("Hi");
//        words.add("Hi");
//        words.add("Hello");
//
//        Map<String, Integer> map = new HashMap<>();
//        for (String s : words) {
//            map.put(s, map.containsKey(s) ? map.get(s) + 1 : 1);
//        }
//        System.out.println(map);



}
