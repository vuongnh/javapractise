package IOAndConcurrent;

import java.util.*;
import java.util.stream.Collectors;

public class MapUntil {

    public static void main(String[] args) {
        Map<String,Long> stringLongMap = new HashMap<>();
        stringLongMap.put("vuong", Long.valueOf(1));
        stringLongMap.put("long", Long.valueOf(3));
        stringLongMap.put("Khanh", Long.valueOf(2));
        System.out.println(stringLongMap);
        System.out.println(stringLongMap.entrySet());
        System.out.println(sortByLongValue(stringLongMap));
        System.out.println(sortByValue(stringLongMap));


    }

    // sort by value using generic
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());
        //list.sort((e1,e2) -> e2.getValue().compareTo(e1.getValue())); //tu lon den be

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
      }

      public static Map<String,Long> sortByLongValue(Map<String,Long> map){
        return  map.entrySet()
                .stream()
                .sorted((e1,e2) -> Long.compare(e2.getValue(), e1.getValue())).limit(1) // sx theo thu tu giam dan
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

}

