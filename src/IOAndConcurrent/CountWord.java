package IOAndConcurrent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CountWord {
    public static void main(String[] args) {

        long startTime = System.nanoTime();
        // create fixed thread pool

//        ExecutorService executorService = Executors.newFixedThreadPool(2);
//        // source need to read
//        File files = new File("D:\\javaConcurren\\Gutenberg");
//        //WordCountImpl.listFileFromDirectory(files); // list filepaths of all file from source directory function
//        List<String> filePaths = new ArrayList<>(); /// list file paths
//        List<WordCountImpl> wordCounts = new ArrayList<>(); // list object callable
//
//        List<Future<Map<String,Integer>>> resultSubmit = new ArrayList<>(); // result of each submit
//        Map<String,Integer> finalRs = new HashMap<>(); // final result
//
//        // get all filepath of files in a directory
//        try(Stream<Path> path = Files.walk(files.toPath())){
//             filePaths = path.filter(Files::isRegularFile).map(x -> x.toString()).collect(Collectors.toList());
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        // add list object callable
//        for (String filePath : filePaths){
//            WordCountImpl wordCount = new WordCountImpl(filePath);
//            wordCounts.add(wordCount);
//        }
//        for (int i = 0; i< wordCounts.size(); i++){
//
////            Future<Map<String,Integer>> future = executorService.submit(wordCounts.get(i));
////            resultSubmit.add(future);
//            resultSubmit.add(executorService.submit(wordCounts.get(i))); // làm tắt
//
//        }
//
//        // resultSubmit = executorService.invokeAll(wordCounts); insteal above for loop
//        // tổng hợp kết quả của các submit
//
//        for (Future<Map<String, Integer>> mapFuture : resultSubmit) {
//            try {
//                Map<String, Integer> temp = mapFuture.get();
//
//                temp.forEach((word, numberOfWord) -> {
//                    if (finalRs.containsKey(word)) {
//                        finalRs.put(word, finalRs.get(word) + numberOfWord);
//                    } else {
//                        finalRs.put(word, numberOfWord);
//                    }
//                });
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                System.out.println(e.getMessage());
//            }
//
//        }
//
////        resultSubmit.forEach(mapFuture -> {
////            Map<String,Integer> rsSubmit = mapFuture.get();
////        });

        Map<String, Integer> finalResult = new HashMap<>();
        String pathDir = "D:\\javaConcurren\\Gutenberg";
        int numberOfThread = 4;
        List<String> listFilePathFromDir = null;
        List<Future<Map<String, Integer>>> resultExecuteThreads = null;
        try {
            listFilePathFromDir = identifyFilePathsFromDir(pathDir);

            resultExecuteThreads = executeCountWordFromDir(numberOfThread, listFilePathFromDir);

            finalResult = sumResult(resultExecuteThreads);
        } catch (IOException e) {
            System.out.println("Có lỗi đọc file");
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Có lỗi xay ra");
        }


        System.out.println(finalResult.keySet().size());
        System.out.println("total word is : " + WordCountImpl.atomicLong);
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("Total time running : " + totalTime);

    }

    private static Map<String, Integer> sumResult(List<Future<Map<String, Integer>>> resultExecuteThreads) throws ExecutionException, InterruptedException {

        Map<String,Integer> finalResult = new HashMap<>();
        for (Future<Map<String, Integer>> resultExecuteThread : resultExecuteThreads) {
            Map<String, Integer> resultOfEachFile = resultExecuteThread.get();
            resultOfEachFile.forEach((word, numberOfWord) ->
                    finalResult.put(word, finalResult.containsKey(word)
                            ? finalResult.get(word) + numberOfWord
                            : numberOfWord)
            );
        }

        return finalResult;
    }

    private static List<String> identifyFilePathsFromDir(String pathDir) throws IOException {
        File fileByPath = new File(pathDir);

        Stream<Path> path = Files.walk(fileByPath.toPath());

        return path
                .filter(Files::isRegularFile)
                .map(Path::toString)
                .collect(Collectors.toList());
    }

    private static List<Future<Map<String, Integer>>> executeCountWordFromDir(int numberOfThreadInPool, List<String> listFilePath) throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(numberOfThreadInPool);

        List<WordCountImpl> listWordCountImpl = listFilePath
                .stream()
                .map(WordCountImpl::new)
                .collect(Collectors.toList());

        return threadPool.invokeAll(listWordCountImpl);
    }

}
