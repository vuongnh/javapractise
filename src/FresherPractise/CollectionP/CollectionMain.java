package FresherPractise.CollectionP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CollectionMain {

    public static void main(String[] args) {

        Student s1 = new Student(1,"Vương");
        Student s2 = new Student(2,"Cuong");
        Student s3 = new Student(3,"Tung");
        Student s4 = new Student(4,"Khanh");

        List<Student> l1 = new ArrayList<>();
        l1.add(s1);l1.add(s3);l1.add(s2);l1.add(s4);
        System.out.println("truocws khi sort ---------");
        l1.forEach(student -> System.out.println(student.getName()));
        // SORT USING COMPARATABLE
//        Collections.sort(l1);
//
//        l1.forEach(student -> System.out.println(student.getName()));

        // SORT USING COMPARATOR
        System.out.println("sort by name using comparator");
        Comparator<Student> sortByName = (student1,student2) -> student1.getName().compareTo(student2.getName());

        Collections.sort(l1,sortByName);
        l1.forEach(student -> System.out.println(student.getName()));

        System.out.println("sort by id using comparator");
        Comparator<Student> sortById = new Comparator<Student>() {
            @Override
            public int compare(Student st1, Student st2) {
                if (st1.getId() == st2.getId())
                    return 0;
                else if (st1.getId() > st2.getId())
                    return 1;
                else
                    return -1;
            }
        };

        Collections.sort(l1,sortById);
        l1.forEach(student -> System.out.println(student.getName()));

    }
}
