package FresherPractise.OOP;

public class Animal {
    public String name="Micky";
    protected float witch;
    private int age;
    public static final String sex; // sử dụng static final để tiết kiệm bộ nhớ lưu trữ

    static{
        sex = "abc";
    }

    public void speaking(){
        System.out.println(" Animal speaking !");
    }

    public void eat(){
        System.out.println("animal eating");
    }
}
