package FresherPractise.Generics;

import java.lang.reflect.InvocationTargetException;

public class Pair<K,V> {
    private K key;
    private V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
    public Pair(Class<K> kClass, Class<V> vClass ) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        this.key = (K)kClass.getDeclaredConstructor().newInstance();
        this.value = (V) vClass.getDeclaredConstructor().newInstance();
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
