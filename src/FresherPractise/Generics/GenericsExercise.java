package FresherPractise.Generics;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class GenericsExercise {

    // bai 1: count number of generic in collection

    public static <T> int countOddNumber(Collection<T> collections, UnaryPredicate<T> up){
        int count = 0;
        Iterator<T> iterator = collections.iterator();

        while (iterator.hasNext()){
            T element = iterator.next();
            if (up.checkOddNumber(element)){
                ++count;
            }
        }
        return count;
    }

    // bai 2
            //    public static <T> T max(T x, T y) {
            //        return x > y ? x : y;
            //    }

        // answer: cant compile because (>) operator only for primitive numeric types

    // bai 3: swap position of 2 element in a array

    public static <T> void swap(T[] array, int e1, int e2){
        if(e1< (array.length - 1) && e2< (array.length -1 )){
            T temp = array[e1];
            array[e1] = array[e2];
            array[e2] = temp;
        }
    }

    // bai 4
    // bai 7
    public static void print(List<? extends Number> list) {
        for (Number n : list) // Interger n :list ==> compile error
            System.out.print(n + " ");
        System.out.println();
    }
    // bai 8 : Write a generic method to find the maximal element in the range [begin, end) of a list.

    public static <T extends Object & Comparable<? super T>> T findMax(List<T> list, int begin, int end){
        T maxValue = list.get(begin);
        for(++begin; begin < end; begin++){
             if (maxValue.compareTo(list.get(begin)) < 0){
                 maxValue = list.get(begin);
             }
        }
        return maxValue;
    }
    // ?? Taij sao can extends them Object
}
