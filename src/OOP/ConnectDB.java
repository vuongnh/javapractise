package OOP;

import java.sql.*;
import java.util.List;

public class ConnectDB {


    private static  String URL_BASE = "jdbc:mysql://localhost:3306/";//studentmanager

    private static Connection conn = null;
    private Statement st = null;
    private ResultSet rs = null;


   public static Connection getConnection(String databaseName, String username, String password){
       if(conn==null){

           try {
               Class.forName("com.mysql.jdbc.Driver");
               conn = DriverManager.getConnection(URL_BASE + databaseName,username,password);
               System.out.println("Connect database thành công !");
           } catch (SQLException  e) {
               System.out.println("Kiểm tra lại thông tin của dataBase !");
               e.printStackTrace();
           }catch (ClassNotFoundException ex){
               System.out.println("Class for Name sai !");
               ex.getStackTrace();
           }
       }else{
           System.out.println("Đã connect database !");
       }
       return conn;

   }
    public void showAllTable(){
        conn = getConnection("jdbcbase","root","vuong");
        String sql = "show tables";

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()){
                System.out.println(rs.getString(1));
            }
        } catch (Exception e) {
            System.out.println(" Lỗi statement !!");
            e.printStackTrace();
        }
        shutdownConnection();

    }

    private void shutdownConnection(){
        try{
            if(rs != null){
                rs.close();
            }
            if(st != null){
                st.close();
            }
            if(conn != null){
                conn.close();
            }
        }catch(SQLException ex){
            ex.getStackTrace();
        }
    }

}
