package OOP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class StudentDao {

    Connection conn = ConnectDB.getConnection("jdbcbase","root","vuong");

    PreparedStatement pt = null;


    public void insertStudent(Student student){

        String sql = "INSERT INTO student (sname, birthday, address) VALUES (?, ?, ?)";
//        String sql = "INSERT INTO student (sname, birthday, address) VALUES ('"
//                + student.getFirstName()+student.getLastName() + "','" +
//                student.getBirthday() + "','" + student.getPlacetoLive() + "')";
        try {

            pt = conn.prepareStatement(sql);
            pt.setString(1,student.getFirstName() + student.getLastName());
            pt.setString(2,student.getBirthday());
            pt.setString(3,student.getPlacetoLive());
            pt.executeUpdate();

            conn.commit();
            System.out.println("Thêm student thành công !!");
        } catch (SQLException e) {
            System.out.println("lỗi prepare statement");
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException ex) {
                System.out.println("Lỗi rollback");
                ex.printStackTrace();
            }
        }finally {
            try{
                if(pt != null){
                    pt.close();
                }
                if(conn != null){
                    conn.close();
                }
            }catch (SQLException ex){
                ex.getStackTrace();
            }
        }
    }

}
