package OOP;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Student extends People{

    private int id;
    private Map<Subject,Float> subjectAndMark;

    public Student() {
    }

    public Student(String firstName, String lastName, Date dateOfBirth, String placetoLive, int id, Map<Subject, Float> subjectAndMark) {
        super(firstName, lastName, dateOfBirth, placetoLive);
        this.id = id;
        this.subjectAndMark = subjectAndMark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<Subject, Float> getSubjectAndMark() {
        return subjectAndMark;
    }

    public void setSubjectAndMark(Map<Subject, Float> subjectAndMark) {
        this.subjectAndMark = subjectAndMark;
    }

    @Override
    public String toString() {
        return "Student Info: first name : " + getFirstName() + " ,Last name : " +
                getLastName() + " ,Birthday : " + getBirthday() + " ,Place of live : " + getPlacetoLive();
    }
}
