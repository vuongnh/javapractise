package OOP;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Teacher extends People {


    private Map<Subject, Map<Student,Float>> studentScoreList;

    public Map<Subject, Map<Student, Float>> getStudentScoreList() {
        return studentScoreList;
    }

    public void setStudentScoreList(Map<Subject, Map<Student, Float>> studentScoreList) {
        this.studentScoreList = studentScoreList;
    }

    @Override
    public String toString() {
        return "Teacher Info: first name : " + getFirstName() + " ,Last name : " +
                getLastName() + " ,Birthday : " + getDateOfBirth() + " ,Place of live : " + getDateOfBirth();
    }
}

