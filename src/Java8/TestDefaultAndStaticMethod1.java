package Java8;

public interface TestDefaultAndStaticMethod1 {

    void showDialog();

    default void showDialog2(){
        System.out.println("Hello Default dialog in interface 2");
    }
}
