package Java8;

public interface TestDefaultAndStaticMethod {

    void showDialog();

    default void showDialog2(){
        System.out.println("Hello Default dialog in interface 1");
    }

    static void showDialog3(){
        System.out.println("Hello Static dialog in interface 1");
    }
}
