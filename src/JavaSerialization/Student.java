package JavaSerialization;

import java.io.Serializable;

public class Student implements Serializable {

    private int id;
    private String name;
    private int age;
    transient String firstName;

    public Student(int id, String name, int age, String firstName) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}
