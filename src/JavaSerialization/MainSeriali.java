package JavaSerialization;

import java.io.*;

public class MainSeriali {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

//        Student s1 = new Student(1,"Nguyễn Thần Long", 23," Huy");
//
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("D:\\javaConcurren\\Gutenberg\\student.txt"));
//        objectOutputStream.writeObject(s1);

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("D:\\javaConcurren\\Gutenberg\\student.txt"));
        Student student = (Student)objectInputStream.readObject();
        System.out.println(student.toString());
    }
}
