package LearningMultiThread.PrimeNumber;

public class PrimeNumber {

    private int count = 0;

    private boolean isPrime(int num){

        if(num >= 2){
        int squartRoot =(int) Math.sqrt(num);
        for(int i = 2; i <= squartRoot; i++){
            if(num%i==0){
                return false;
            }
        }
    }else{
            return false;
        }
        return true;

    }

    public synchronized void findPrimeNumber(int fromNumber, int toNumber){

            for(int i = fromNumber; i <= toNumber; i++){
                try{
                    if(isPrime(i)){
                            System.out.println("số nguyên tố là :" + i);
                            this.wait();
                            System.out.println(count);
                    }
                }catch(InterruptedException ex){
                    System.out.println(ex.toString());
                    }
            }

        System.out.println("Tổng số prime number là : " + count);
    }

    public synchronized void countPrimeNumber(){
        System.out.println("Vào luồng tính count !");
        count++;
        this.notifyAll();
    }
}
