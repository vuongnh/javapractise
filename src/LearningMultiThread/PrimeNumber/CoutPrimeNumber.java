package LearningMultiThread.PrimeNumber;

public class CoutPrimeNumber {
    public static void main(String[] args) {

        PrimeNumber pr = new PrimeNumber();
        Thread th1 = new Thread(){
            @Override
            public void run() {
                pr.findPrimeNumber(1,100);
            }
        };
        th1.start();
        Thread th2 = new Thread(){
            @Override
            public void run() {
                pr.countPrimeNumber();
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        th2.start();
    }
}
