package LearningMultiThread.TinhTong;

import java.util.Random;

public class TinhTong {

    static DataShare dataShare;

    public static void main(String[] args) {
        dataShare = new DataShare();
        dataShare.setLaCo(1);

        Thread th1 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (dataShare){
                for(int i = 0; i<100;i++){
                    try {
                        if (dataShare.getLaCo() == 1) {
                            int a = new Random().nextInt(100);
                            dataShare.setA(a);
                            System.out.println("A = " + dataShare.getA());
                            Thread.sleep(8);
                            dataShare.setLaCo(2);
                            dataShare.notifyAll();
                        } else {
                            dataShare.wait();
                        }
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
                }
            }
        });
        Thread th2 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (dataShare){
                    for(int i = 0; i<100;i++){
                        try {
                            if (dataShare.getLaCo() == 2) {
                                int a = new Random().nextInt(100);
                                dataShare.setB(a);
                                System.out.println("B = " + dataShare.getB());
                                Thread.sleep(9);
                                dataShare.setLaCo(3);
                                dataShare.notifyAll();
                            } else {
                                dataShare.wait();
                            }
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        Thread th3 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (dataShare){
                    for(int i = 0; i<100;i++){
                        try {
                            if (dataShare.getLaCo() == 3) {
                                System.out.println("Tổng  = " + dataShare.tinhTong());
                                Thread.sleep(8);
                                dataShare.setLaCo(1);
                                dataShare.notifyAll();
                            } else {
                                dataShare.wait();
                            }
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        th1.start();
        th2.start();
        th3.start();
    }
}
