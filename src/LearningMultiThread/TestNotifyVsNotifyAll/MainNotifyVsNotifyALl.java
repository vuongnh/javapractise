package LearningMultiThread.TestNotifyVsNotifyAll;

public class MainNotifyVsNotifyALl {

    public static void main(String[] args) {
        Student student = new Student();

        Student1 student1 = new Student1(student);
        Student2 student2 = new Student2(student);

        StudentNotify studentNotify = new StudentNotify(student);

        Thread thread1 = new Thread(student1);
        Thread thread2 = new Thread(student2);
        Thread thread3 = new Thread(studentNotify);

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
