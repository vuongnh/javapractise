package LearningMultiThread.TestNotifyVsNotifyAll;

public class StudentNotify extends Thread{
    private Student student;

    public StudentNotify(Student student){
        this.student = student;
    }

    @Override
    public void run() {
        synchronized (this.student) {
            this.student.notify();
        }
    }
}
