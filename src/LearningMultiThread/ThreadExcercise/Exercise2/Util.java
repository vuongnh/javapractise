package LearningMultiThread.ThreadExcercise.Exercise2;

import java.util.Random;

public class Util {

    public static int randomInt(int min, int max){
        Random rd = new Random();
        int randomNumber = rd.nextInt(max + 1 - min) + min;

        return randomNumber;
    }
}
