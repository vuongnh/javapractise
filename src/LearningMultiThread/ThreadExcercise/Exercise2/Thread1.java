package LearningMultiThread.ThreadExcercise.Exercise2;

public class Thread1 implements Runnable {


    @Override
    public void run() {
        System.out.println("THread 1 is running !");
        createLamp();
    }
    private void createLamp(){
        Store store = Store.getInstance();

        while(true){
            int numLamp = Util.randomInt(1,20);
            System.out.println("Số lamp đc tạo ra trong thread 1 là  : " + numLamp);
            for(int i =0; i < numLamp; i++){
                int randomStatus = Util.randomInt(0, Status.values().length -1);
                Lamp lamp = new Lamp(Status.values()[randomStatus]);
                store.add(lamp);
                System.out.println("THread 1 just add a lamp to Store : " + lamp.toString());

                try {
                    Thread.sleep(100);
                    System.out.println("THread 1 jusst sleep 100ms");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }


    }
}
