package LearningMultiThread.ThreadExcercise.Exercise2;

import java.util.concurrent.CopyOnWriteArrayList;

public class Trash extends CopyOnWriteArrayList<Lamp> {
    private static Trash instance;

    public static synchronized Trash getInstance(){
        if(instance == null) {
            instance = new Trash();
        }
        return instance;
    }

}
