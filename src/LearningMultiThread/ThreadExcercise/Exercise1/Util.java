package LearningMultiThread.ThreadExcercise.Exercise1;

import java.util.Random;

public class Util {

    public static int randomInt(int min, int max){
        Random rd = new Random();
        int randomNumber = rd.nextInt(max + 1 - min) + min;

        return randomNumber;
    }
}
