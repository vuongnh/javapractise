package OCA2;

import java.util.ArrayList;
import java.util.List;

public class CodeBlock {
    public static void main(String[] args) {

        int a[] = {1,2,3};
        List<String> a1 = new ArrayList<>();
        a1.add("vuong");
        System.out.println(a);
        System.out.println(a1);

        long max = 3123456789L;

        System.out.println(max);

//        double notAtStart = _1000.00; // DOES NOT COMPILE
//        double notAtEnd = 1000.00_; // DOES NOT COMPILE
//        double notByDecimal = 1000_.00; // DOES NOT COMPILE
        double annoyingButLegal = 1_00_0.0_0; // this one compiles

        System.out.println(annoyingButLegal);
    }

}
