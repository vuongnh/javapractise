package OCA2;

public class StringMain {
    public static void main(String[] args) {

        System.out.println(1+2+"abc"+(4+3));

        String s1 = "abc";
        s1+="def1";
        s1.concat("def2");
        System.out.println(s1);

        String a = "abc";
        String b = a.toUpperCase();
        b = b.replace("B","2").replace("C","3");
        System.out.println(a);
        System.out.println(b);


        // substring return a string so a1 dont change
        String a1 = "vuongnh";
        System.out.println(a1.substring(4));
        System.out.println(a1);

        String a2 = "vuongnh1";
        a2.toUpperCase();
        System.out.println(a2);

        StringBuilder sb = new StringBuilder("abc");
        StringBuilder sb1 = sb.append("de");
        sb1.append("f").append("g");
        sb1.append('-').append(true);
        System.out.println(sb);
        System.out.println(sb1);
        //System.out.println(sb1.charAt(10));

        int i = 0;
        i = i++;
        System.out.println(i);
        i=++i;
        System.out.println(i);

        String st1 = "vuongnh";
        String st2 = "vuongnh".trim();
        System.out.println();
        System.out.println(st1==st2);


        String number  = "0123456789";
        System.out.println("==============" + number.substring(7,7));

        String sst = " vuongnh ";
        sst.trim();
        sst +=2;
        sst+=true;
        System.out.println("========="+sst);

        StringBuilder ssb1 = new StringBuilder("0123456789");
        ssb1.substring(1,3);
        //ssb1.substring(6,5); //StringIndexOutOfBoundsException
        System.out.println(ssb1.length());



        String stt2 = "vuongn";
        stt2 +="h"; // concat make change memory address of stt2
        //stt2 = stt2.concat("h");
        System.out.println(stt2);
        String stt3 = "vuongnh";
        System.out.println(stt2==stt3);



    }
}
