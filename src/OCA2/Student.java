package OCA2;

import java.io.FileNotFoundException;
import java.rmi.server.ExportException;

public class Student {

    protected String name;

    private int age;

    public Student() {
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student(String name) {
        //this.name = name;
        //System.out.println(" this() in constructor must be the first except cmt");
        this("vuongnh",4);
    }


    public int getAge(){
        Integer age = 8;
        ///Long age = 8l; // compile fail
        //return 9.0;


        return age;
    }
}
