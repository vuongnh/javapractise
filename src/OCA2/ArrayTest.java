package OCA2;

import static java.lang.Math.*;

public class ArrayTest {

    public static void main(String[] args) {
        System.out.println(PI);

        int arr[][][] = {{{1,3,4},{5,6,8},{3,4,9}},{{10,3,4},{5,6,8},{3,4,9}}};

        System.out.println(arr[1][0][0]);
        int arr2[][][] = {{{0,3,4},{5,6,7}}};

        int arr1[][][] = new int[1][2][3]; // gồm 1 mảng 2 chiều. mảng 2 chiều đó có 2 mảng 1 chiều. mỗi mảng 1 chiều có 3 phần tử
        arr1[0][0][0] = 0;
        arr1[0][0][1] = 1;
        arr1[0][0][2] = 2;
        arr1[0][1][0] = 3;
        arr1[0][1][1] = 4;
        arr1[0][1][2] = 5;
//        arr1[0][2][0] = 6;
//        arr1[0][2][1] = 7;
//        arr1[0][2][2] = 8;

//        System.out.println(arr2[0][2][2]); ==> index 2 out
        System.out.println(arr2[0][1][2]);
//        arr1[1][0][0] = 0;
//        arr1[1][0][1] = 1;
//        arr1[1][0][2] = 2;

    }
}
