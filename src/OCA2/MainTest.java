package OCA2;

// inner class cant have static declare
public class MainTest {

    public static void main(String[] args) {
        System.out.println("Main 1");
        Student student = new Student();
        System.out.println(student.name);

    }

    public static void main(String a){
        System.out.println("Main 2");
    }
//    public static void main(Float... a){
//
//    }


}
