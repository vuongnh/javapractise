package OCA2;

public class Whiz {
    public Whiz() {
        number = 5;
    }
    public static void main(String[] args) {
        Whiz egg = new Whiz();
        System.out.println(egg.number);
    }

    { number = 4; }
    private static int number = 3;
}
