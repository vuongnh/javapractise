package OCA2.FromChapter4;

import OCA2.Student;

import java.util.ArrayList;
import java.util.List;

public class AccessModifierMain{

    private int age;

//    private final int age1 = 1;
//    {
//        age1 = 2;
//    }
//    private static final int age1;
//    static {
//        age1 = 7;
//    }
    private String name = "vuongnh";
    public void one(){
        System.out.println("one");
        age = 0;
    }
    public static void main(String[] args) {

        new AccessModifierMain().one();
        new AccessModifierMain().overload(2,4);
        new AccessModifierMain().castOverload("abc");

        List<String> aa = new ArrayList<>();
        aa.removeIf(s -> s.charAt(0) != 'h');
        try

        {

            int x = 0;

            int y = 5 / x;

        }

        catch (ArithmeticException e)

        {

            System.out.println("Exception");

        }

        catch (Exception e)

        {

            System.out.println(" Arithmetic Exception");

        }

        System.out.println("finished");
    }

    public void overload(int a, int b){
        System.out.println("overload 2 int param");
    }
    public void overload(int... a){
        System.out.println("varagrs overload");
    }

    public void castOverload(Object object){
        System.out.println("object method");
    }

}
