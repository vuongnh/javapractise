package OCA2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayMain {

    public static void main(String[] args) {

        // Khỏi tạo lại là trỏ(references) đến một mảng khác
        String[] st1 = new String[2];
        System.out.println(st1.toString());
        st1[0] = "vuongnh";
        System.out.println(st1[0]);
        st1 = new String[3];
        System.out.println(st1.toString());
        System.out.println(st1.length);
        System.out.println(st1[0]);

        int[] a = {2,1,9};
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));

        // search với sorted array
        // without sorted array ==> results will be is unpredictable
        int[] a1 = {2,4,6,8,10};
        System.out.println(Arrays.binarySearch(a1,9));
        System.out.println(Arrays.binarySearch(a1,5));
        int[] a2 = {2,1,6,8,10};
        System.out.println(Arrays.binarySearch(a2,3)); // unpredictable as a2 is unsorted

        //equal 2 array
        List<String> st2 = new ArrayList<>();
        List<String> st3 = new ArrayList<>();
        st2.add(0,"vuongnh");
        st2.add(1,"bangnh");
        st3.add(0,"vuongnh");
        st3.add(1,"congnh");
        System.out.println(st2.equals(st3));


        List<String> list1 = new ArrayList<String>();

        //int[][] java = new int[][]; //////havent init yet

        List<String> list = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        list.add("vuongnh");
        list.add("bangnh");
        //list.remove(0);
        list2.add("vuongnh");
        list2.add("bangnh");
        System.out.println(list.equals(list2));

        String[] strings1 = {"vuongnh","bangnh"};
        String[] strings2 = {"vuongnh","bangnh"};
        System.out.println(strings1.equals(strings2));

        List<Integer> integers = Arrays.asList(1,4,6,2);
        Integer aa[] = integers.toArray(new Integer[3]);
        Object[] aaa = integers.toArray();
        System.out.println(Arrays.toString(aa));
        System.out.println(Arrays.toString(aaa));

        List<Integer> integers1 = new ArrayList<>();
        integers1.add(6);
        integers1.add(null);
        integers1.add(5);
        //for (int i : integers1) System.out.println(i);
        System.out.println(integers1.size());
        for (int i = 0; i < integers1.size(); i++) {
            System.out.println(integers1.get(i));
        }

    }
}
