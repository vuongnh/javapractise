package OCA2;

import org.springframework.web.servlet.support.JstlUtils;

public class OperatorMain {
    static Long a7;
    static int a6;
    static Brand brand;
    static String name;

    public static void main(String[] args) {

        char a = 4;
        char b = 2;
        int c = a/b; // char byte short auto convert to int

        System.out.println(c);

        short x = 14342;
        //float a6 = 1.2; compile fail
        float a1 = 1.2f;
        float y = 13; // float can be init by int by cant init by 1.2 without f after
        double z = 30;

        // explain overflow in datatype in
        int a2 = 2147483647;
        int a3 = 1;
        int a4 = (a2+a3)/2; // overflow
        int a5 = a3 + (a2-a3)/2;
        System.out.println(a4);
        System.out.println(a5);

        System.out.println(a7);
        a6 = 2;
       switch (a6) {
           case 2:
               System.out.println("two");
               break;
           default:
               System.out.println("default");
               break;
       }

       brand = Brand.HAWEI;

       // the different between if-else and switch-case
        // chạy bắt đàu từ case đúng
       switch (brand){
           default:
               System.out.println("not brand");
               break;
           case APPLE:
               System.out.println("apple");
               break;
           case SONY:
               System.out.println("sony");
               break;
           case SAMSUNG:
               System.out.println("samsung");
               break;
       }

//        for (int i = 0;i < 2;){
//            System.out.println("===========");
//        }
        //System.out.println(" =============");

        name = "vuong";
        switch (name){
            case "vuong":
                System.out.println("vuong");
                break;
            default:
                System.out.println("def ");
                break;
        }


    }
}
