package ExtractDataFromTextFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JobImplement {

    private String originPath;
    private String dicPath;
    private String stringInALine;

    public JobImplement(String originPath, String dicPath, String stringInALine) {
        this.originPath = originPath;
        this.dicPath = dicPath;
        this.stringInALine = stringInALine;
    }

    public void implement() throws IOException {

        List<String> lines = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(this.originPath))) {

            lines = stream
                    .filter(line -> line.contains(this.stringInALine))
                    .map(line->line+"\r\n \r\n")
                    .collect(Collectors.toList());
//            stream.filter(line->line.contains(this.stringInALine))
//                    .forEach(line -> {
//                        System.out.println(line);
//            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String line: lines) {
            Files.writeString(Paths.get(this.dicPath),line, StandardOpenOption.CREATE,StandardOpenOption.APPEND);
        }

    }
}
