package exercise;

public class ReverseNumber {
    //vi du số : 1234
    public void reverseNumber(int number){
        int reverseNumber = 0;
        int temp = 0;
        while(number>0){
            temp = number%10;
            reverseNumber = reverseNumber*10 + temp;
            number = number/10;
        }
        System.out.println("số đảo ngược của số " + number + " là " + reverseNumber);
    }
}
