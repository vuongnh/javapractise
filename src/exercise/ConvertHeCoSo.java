package exercise;

import java.util.ArrayList;

public class ConvertHeCoSo {

    public void DecToHex(int num){
       int du;
       String hex = "";
       char[] hexchar = {'0','1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
       while (num>0){
            du = num%16;
            hex= hexchar[du] + hex;
            num= num/16;
       }
        System.out.println("số thập phân đc chuyển thành số thập lục phân là : " + hex);
    }
    public void DectoBin(int num) {
        int du;
        String bin = "";
        char[] binchar = {'0','1'};
        while(num>0){
            du=num%2;
            bin = binchar[du]  + bin;
            num=num/2;
        }
        System.out.println("số thập phân được chuyển thành số nhị phân là : " + bin);
    }
    public void BintoDec(int bin){
        int decimal=0;
        int n=0;
        while(true){
            if(bin ==0){
                break;
            }else{
                int temp = bin%10;
                decimal += temp*Math.pow(2,n);
                bin=bin/10;
                n++;
            }
        }
        System.out.println("số thập phân được chuyển từu số nhị phân trên là : " +  decimal);
    }
}
