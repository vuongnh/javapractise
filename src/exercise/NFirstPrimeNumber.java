package exercise;

public class NFirstPrimeNumber {
    public boolean isPrimeNumber(int n){
        if(n<2){
            return false;
        }
        int squartRoot = (int)Math.sqrt(n);

        for(int i = 2 ; i <= squartRoot ; i++ ){
            if(n%i==0) return false;
        }
        return true;
    }

    public void findNFisrtPrimeNumber(int n){
        int countPrimeNumber = 0;
        int i = 2;
        while(countPrimeNumber<n){
            if(isPrimeNumber(i)){
                System.out.println(i+",");
                countPrimeNumber++;
            }
            i++;
        }
    }
}
