package exercise;

import java.util.Arrays;

public class HoanDoiTwoNumbr {
    public void hoandoi(int[] a){

        if(a.length==2){
            System.out.println("Thứ tự ban đầu của 2 phần tử là : " + Arrays.toString(a));
            a[0] = a[0]+a[1];
            a[1] = a[0] -a[1];
            a[0] = a[0] -a[1];
            System.out.println("Thứ tự ban đầu của 2 phần tử là : " + Arrays.toString(a));
        }
    }
}
