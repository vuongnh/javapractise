package exercise;

import java.net.InetAddress;

public class GetIpAddress {
    public void getIpAddress() throws Exception{
        InetAddress myIp = InetAddress.getLocalHost();
        System.out.println("Địa chỉ ip hiện tại của máy là : " + myIp.getHostAddress());

    }
}
