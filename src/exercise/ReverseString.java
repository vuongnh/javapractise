package exercise;

import java.util.Scanner;

public class ReverseString {
    public void reverseString(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Mời nhập chuỗi cần đảo ngược : ");
        String chuoi1 = sc.nextLine();
        String  chuoi2 = new StringBuffer(chuoi1).reverse().toString();
        System.out.println(chuoi2);
    }
}
