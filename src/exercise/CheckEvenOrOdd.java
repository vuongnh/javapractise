package exercise;

public class CheckEvenOrOdd {
    public void check(int a){
        if(a%2==0){
            System.out.println(a + " is even number");
        }else{
            System.out.println(a + " is odd number");
        }
    }
}
