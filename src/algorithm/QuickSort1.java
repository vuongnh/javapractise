package algorithm;

import java.util.Arrays;

/// rightmost is pivot
public class QuickSort1 {
    public static void main(String[] args) {
        int[] a = {25,32,56,32,65};
        quickSort(a,0,a.length-1);
        System.out.println(Arrays.toString(a));
    }
    public static void quickSort(int[] arr, int start, int end){

        int partition = partition(arr,start,end);
        if (partition-1>start){
            quickSort(arr,start,partition-1);
        }
        if (partition+1<end){
            quickSort(arr,partition+1,end);
        }

    }

    public static int partition(int[] arr, int start, int end){
        int pivot = arr[end];

        for (int i = start; i < end; i++){
            if (arr[i] < pivot){
                int t = arr[i];
                arr[i] = arr[start];
                arr[start] = t;
                start++;
            }
        }

        int t = arr[start];
        arr[start] = pivot;
        arr[end] = t;

        return start;
    }
    public static void swap(int a, int b){
        System.out.println("a :" + a + " b: " + b);
        a =a+b;
        b = a-b;
        a = a-b;
        System.out.println(" Sau khi swap a : " + a + " b: " +b);
    }
}
