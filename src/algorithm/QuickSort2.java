package algorithm;

import java.util.Arrays;

// sort with pivot as middle element
public class QuickSort2 {

    public static void main(String[] args) {
        int[] a = {25,32,56,32,65,87,39,31,54};
        quickSort(a,0,a.length-1);
        System.out.println(Arrays.toString(a));
    }

    public static void quickSort(int[] a, int low, int high){

        int middle = low + (high - low)/2;
        int pivot = a[middle];
        int i = low; int j = high;
        while (i <= j){
            while (a[i] < pivot){
                i++;
            }
            while(a[j] > pivot){
                j--;
            }
            if (i<=j){
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                i++;
                j--;
            }
        }

        // recursively 2 sub array
        if (low < j){
            quickSort(a,low,j);
        }
        if (high > i){
            quickSort(a,i,high);
        }
    }
}
